from django.utils.translation import ugettext_lazy as _
from django.http import HttpResponse, HttpRequest, HttpResponseRedirect, Http404
from django.shortcuts import render_to_response, get_object_or_404
from django.template.context import RequestContext
from localeurl.models import reverse
from django.conf import settings

from cmsbase.views import page_processor

from faq.models import FAQ, FAQTranslation

def list(request):
	return render_to_response('faq/list.html', {}, context_instance=RequestContext(request))

def view(request, slug):
	_faq_translation = get_object_or_404(faqTranslation, slug=slug, parent__published=True)
	_faq = _faq_translation.parent
	return render_to_response('faq/view.html', {'faq':_faq,}, context_instance=RequestContext(request))
