from django.db import models
from django.conf import settings
from localeurl.models import reverse

from mptt.models import MPTTModel, TreeForeignKey, TreeManyToManyField
from multilingual_model.models import MultilingualModel, MultilingualTranslation
from cmsbase.models import BasePage, PublishTranslation, BasePageManager, Page



class FAQTranslation(MultilingualTranslation):
	parent = models.ForeignKey('FAQ', related_name='translations')
	title = models.CharField('Title', max_length=100)
	slug = models.SlugField(max_length=100)
	content = models.TextField(blank=True)

	class Meta:
		unique_together = ('parent', 'language_code')

		if len(settings.LANGUAGES) > 1:
			verbose_name=u'Translation'
			verbose_name_plural=u'Translations'
		else:
			verbose_name=u'Content'
			verbose_name_plural=u'Content'

	def __unicode__(self):
		return dict(settings.LANGUAGES).get(self.language_code)



class FAQ(MultilingualModel):
	identifier = models.SlugField(max_length=100)
	published = models.BooleanField('Active')
	order_id = models.IntegerField()


	class Meta:
		verbose_name=u'FAQ'
		verbose_name_plural=u'FAQs'
		ordering = ['order_id']
	class CMSMeta:
		translation_class = FAQTranslation

	def __unicode__(self):
		return self.unicode_wrapper('title', default='Unnamed')

	def get_translations(self):
		return self.CMSMeta.translation_class.objects.filter(parent=self)

	def translated(self):
		from django.utils.translation import get_language

		try:
			translation = self.CMSMeta.translation_class.objects.get(language_code=get_language(), parent=self)
			return translation
		except:
			return self.CMSMeta.translation_class.objects.get(language_code=settings.LANGUAGE_CODE, parent=self)

	def get_absolute_url(self):
		return reverse('faq:view', kwargs={'slug':self.translated().slug})