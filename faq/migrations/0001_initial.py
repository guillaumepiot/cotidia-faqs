# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='FAQ',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('identifier', models.SlugField(max_length=100)),
                ('published', models.BooleanField(verbose_name='Active')),
                ('order_id', models.IntegerField()),
            ],
            options={
                'ordering': ['order_id'],
                'verbose_name': 'FAQ',
                'verbose_name_plural': 'FAQs',
            },
        ),
        migrations.CreateModel(
            name='FAQTranslation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('language_code', models.CharField(max_length=7, verbose_name='language', choices=[(b'en', b'English')])),
                ('title', models.CharField(max_length=100, verbose_name='Title')),
                ('slug', models.SlugField(max_length=100)),
                ('content', models.TextField(blank=True)),
                ('parent', models.ForeignKey(related_name='translations', to='faq.FAQ')),
            ],
            options={
                'verbose_name': 'Content',
                'verbose_name_plural': 'Content',
            },
        ),
        migrations.AlterUniqueTogether(
            name='faqtranslation',
            unique_together=set([('parent', 'language_code')]),
        ),
    ]
