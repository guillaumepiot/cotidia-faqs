from django import template
register = template.Library()

from faq.models import FAQ

@register.assignment_tag
def get_all_faqs(page=False):
	faqs = FAQ.objects.filter(published=True).order_by('order_id')
	return faqs