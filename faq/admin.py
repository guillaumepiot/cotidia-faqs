import reversion

from django.contrib import admin
from django import forms
from django.utils.translation import ugettext as _
from django.conf import settings
from django.contrib.admin.views.main import ChangeList

from mptt.admin import MPTTModelAdmin
from multilingual_model.admin import TranslationInline

from redactor.widgets import RedactorEditor

from cmsbase.admin import PageAdmin, PageFormAdmin, PublishingWorkflowAdmin
from cmsbase.widgets import AdminImageWidget, AdminCustomFileWidget

from faq.models import *


# Category translation

class FAQTranslationInlineFormAdmin(forms.ModelForm):
	slug = forms.SlugField(label=_('slug'))
	content = forms.CharField(widget=RedactorEditor(redactor_css="/static/css/redactor-editor.css"), required=False)

	class Meta:
		model = FAQTranslation
		exclude = ()

	def has_changed(self):
		""" Should returns True if data differs from initial.
		By always returning true even unchanged inlines will get validated and saved."""
		return True


class FAQTranslationInline(TranslationInline):
	model = FAQTranslation
	form = FAQTranslationInlineFormAdmin
	extra = 0 if settings.PREFIX_DEFAULT_LOCALE else 1
	prepopulated_fields = {'slug': ('title',)}
	#template = 'admin/cmsbase/cms_translation_inline.html'



# Category

class FAQAdmin(admin.ModelAdmin):

	list_display = ["title", "published", 'order_id', 'languages']
	inlines = (FAQTranslationInline, )

	def title(self, obj):
		translation = obj.translated() #PageTranslation.objects.filter(parent=obj, language_code=settings.DEFAULT_LANGUAGE)
		if translation:
			return translation.title
		else:
			return _('No translation available for default language')

	def languages(self, obj):
		ts=[]
		for t in obj.get_translations():
			ts.append(u'<img src="/static/admin/img/flags/%s.png" alt="" rel="tooltip" data-title="%s">' % (t.language_code, t.__unicode__()))
		return ' '.join(ts)

	languages.allow_tags = True
	languages.short_description = 'Translations'

	# Override the list display from PublishingWorkflowAdmin
	def get_list_display(self, request, obj=None):
		if not settings.PREFIX_DEFAULT_LOCALE:
			return ["title", "published", 'order_id']
		else:
			return ["title", "published", 'order_id', 'languages']

	fieldsets = (

		
		('Settings', {
			#'description':_('The page template'),
			'classes': ('default',),
			'fields': ('published', 'order_id', )
		}),

	)

	class Media:
		css = {
			"all": ("admin/css/page.css",)
		}
		js = ("admin/js/page.js",)

admin.site.register(FAQ, FAQAdmin)