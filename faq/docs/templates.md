Templates
=========

Template tags
-------------


`get_all_faqs` populate the template context with all the FAQs.

	{% get_all_faqs as faqs %}
	
Eg:
	
	{% get_all_faqs as faqs %}
	{% for faq in faqs %}
		{{faq}}
	{% endfor %}

	
	
