from django.conf.urls import patterns, include, url

urlpatterns = patterns('faq',

	# All FAQs
	url(r'^$', 'views.list', name="faqs"),

	# View FAQs
	url(r'^(?P<slug>[-\w\/]+)/$', 'views.view', name="view"),

)